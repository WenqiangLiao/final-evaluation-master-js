function load() {
    seperateCell();

    let winResultArray = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]];

    let playerXResult = [];
    let playerOResult = [];



}

function seperateCell() {
    let allBox = document.getElementsByClassName("boxCell");
    for(let i = 0; i < allBox.length; i++) {
        allBox[i].onmouseover = mouseOnCell;
        allBox[i].onmouseout = mouseOutCell;
        allBox[i].onmousedown = mouseDownCell;
    };
}

function mouseOnCell() {
    this.style.backgroundColor = "lightgrey";
}
function mouseOutCell() {
    this.style.backgroundColor = "white"
}

function mouseDownCell() {
    this.style.backgroundColor = "darkgrey";
    let target = this;

    let hintValue = document.getElementById("playerHint").innerHTML.substr(-1);
    setTimeout(() => print(target, hintValue), 300);

    let playerHint = document.getElementById("playerHint");
    let newHintValue = hintValue === "X" ? "O" : "X";
    playerHint.innerHTML = `Next Round: ${newHintValue}`;

    let winStatus = cellIndentifyAdd(hintValue, target);
}

function print(target, hintValue) {
    let printValue = hintValue === "X" ? "X" : "O";
    target.innerHTML = `${printValue}`;
    target.style.backgroundColor = "white";
}

function cellIndentifyAdd(hintValue, target) {
    let indentify = getCellIndentify(target)
    if(hintValue === "X") {
        playerXResult.push(indentify);
        gameJudgement(playerXResult, winResultArray);
    } else {
        playerOResult.push(indentify);
        gameJudgement(playerOResult, winResultArray);
    }
}

function getCellIndentify(target) {
    let indentify = parseInt(target.id);
    return indentify;
}

function gameJudgement(playerResult, winResultArray) {
    for(array in winResultArray) {
        if(playerResult.includes(array)){
            return true;
        }
    }
    return false;
}
